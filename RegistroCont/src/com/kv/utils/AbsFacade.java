package com.kv.utils;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kz
 */
public abstract class AbsFacade<T> {

	private Class<T> entityClass;

	public AbsFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Autowired
	public abstract SessionFactory getSessionFactory();

	protected Session session;

	@Transactional
	public void create(T entity) {
		session = getSessionFactory().getCurrentSession();
		session.save(entity);
	}

	@Transactional
	public void update(T entity) {
		session = getSessionFactory().getCurrentSession();
		session.update(entity);
	}

	@Transactional
	public void delete(T entity) {
		session = getSessionFactory().getCurrentSession();
		session.delete(entity);
	}

	@javax.transaction.Transactional
	public T readById(Object id) {
		session = getSessionFactory().getCurrentSession();
		return session.find(entityClass, id);
	}

	@Transactional
	public List<T> read() {
		session = getSessionFactory().getCurrentSession();
		CriteriaQuery cq = session.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return session.createQuery(cq).getResultList();
	}

}