package com.kv.utils;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.kv.impl.AsientoDao;
import com.kv.impl.CuentaDao;
import com.kv.impl.DetalleasientoDao;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableWebMvc
@ComponentScan({"com.kv"}) // Se llaman los paquetes de utilizacion del proyecto
public class Configuracion {

	@Bean
	public InternalResourceViewResolver // componente que resuelve la integracion
			vistas() {
		InternalResourceViewResolver resultado = new InternalResourceViewResolver();
		resultado.setPrefix("/");
		resultado.setSuffix(".jsp");
		return resultado;
	}

	@Bean(name = "datasource")
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://192.168.101.91:3306/registro?useSSL=false");
		dataSource.setUsername("kz");
		dataSource.setPassword("kzroot");
		return dataSource;
	}

	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource datasource) {
		LocalSessionFactoryBuilder fabrica = new LocalSessionFactoryBuilder(datasource);
		fabrica.scanPackages("com.kv.models");
		fabrica.addProperties(getHibernateProperties());
		return fabrica.buildSessionFactory();
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager tx = new HibernateTransactionManager(sessionFactory);
		return tx;
	}

	private Properties getHibernateProperties() {
		Properties hibernateProperties = new Properties();
		// hibernateProperties.put("hibernate.show_sql", "true");
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return hibernateProperties;
	}

	
	@Autowired
	@Bean(name = "AsientoDao")
	public AsientoDao asientoDao(SessionFactory sessionFactory) {
		return new AsientoDao(sessionFactory);
		
	}
	@Autowired
	@Bean(name = "CuentaDao")
	public CuentaDao cuentaDao(SessionFactory sessionFactory) {
		return new CuentaDao(sessionFactory);
		
	}
	@Autowired
	@Bean(name = "DetalleasientoDao")
	public DetalleasientoDao detalleasientoDao(SessionFactory sessionFactory) {
		return new DetalleasientoDao(sessionFactory);
		
	}
}