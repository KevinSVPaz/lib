package com.kv.impl;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kv.utils.AbsFacade;
import com.kv.utils.Dao;
import com.kv.models.Cuenta;

@Repository
@Transactional
public class CuentaDao extends AbsFacade<Cuenta> implements Dao<Cuenta> {

	@Autowired
	SessionFactory startSession;

	public CuentaDao(SessionFactory sessionFactory) {
		super(Cuenta.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}
	
	/* Metodos especificos */
}