package com.kv.impl;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kv.utils.AbsFacade;
import com.kv.utils.Dao;
import com.kv.models.Cuenta;
import com.kv.models.DetalleAsiento;

@Repository
@Transactional
public class DetalleasientoDao extends AbsFacade<DetalleAsiento> implements Dao<DetalleAsiento> {

	@Autowired
	SessionFactory startSession;

	public DetalleasientoDao(SessionFactory sessionFactory) {
		super(DetalleAsiento.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}
	
	/* Metodos especificos */
}