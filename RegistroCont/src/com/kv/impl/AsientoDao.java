package com.kv.impl;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kv.utils.AbsFacade;
import com.kv.utils.Dao;
import com.kv.models.Asiento;

@Repository
@Transactional
public class AsientoDao extends AbsFacade<Asiento> implements Dao<Asiento> {

	@Autowired
	SessionFactory startSession;

	public AsientoDao(SessionFactory sessionFactory) {
		super(Asiento.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}

	/* Metodos especificos */

	public Asiento consultarUltimoAsiento() {
		Asiento a = new Asiento();
		List<Asiento> lista = new LinkedList<>();
		String sql = "Select * from registro.asiento a order by a.id_asiento desc ";
		try {
			Query q = getSessionFactory().getCurrentSession().createNativeQuery(sql, Asiento.class);
			if (q.getResultList().isEmpty()) {
				lista = q.getResultList();
				a = lista.get(0);
			}
		} catch (Exception e) {

			a = new Asiento();
		}
		return a;
	}
}