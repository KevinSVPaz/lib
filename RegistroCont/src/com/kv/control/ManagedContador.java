package com.kv.control;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.frt.models.Facturas;
import com.frt.models.Ordenes;
import com.kv.impl.AsientoDao;
import com.kv.impl.CuentaDao;
import com.kv.impl.DetalleasientoDao;
import com.kv.models.Asiento;
import com.kv.models.Cuenta;
import com.kv.models.DetalleAsiento;

@Controller
public class ManagedContador {
	@Autowired
	@Qualifier("AsientoDao")
	private AsientoDao asientoDao;
	@Autowired
	@Qualifier("CuentaDao")
	private CuentaDao cuentaDao;
	@Autowired
	@Qualifier("DetalleasientoDao")
	private DetalleasientoDao detalleasientoDao;

	private String error;
	private String msg;
	private String vista;

	@RequestMapping(value = "/verCuentas", method = RequestMethod.GET)
	public ModelAndView consultarCuenta() {
		ModelAndView mv = new ModelAndView("cuenta");
		try {
			msg = "Cuentas";
			List<Cuenta> lista = new LinkedList<>();
			lista = cuentaDao.read();
			mv.addObject("msg", msg);
			mv.addObject("lista", lista);
			mv.setViewName("cuenta");
			return mv;
		} catch (Exception e) {
			System.out.println("error: " + e);
			error = "error al en tablaCuentas desde " + this.getClass().getSimpleName();
			this.vista = "cuenta";
			this.errorHand(this.vista);
			return mv;
		}
	}

	@RequestMapping(value = "/verDetalleAsiento", method = RequestMethod.GET)
	public ModelAndView consultarDetalleAsiento() {
		ModelAndView mv = new ModelAndView("detalleasiento");
		try {
			msg = "Detalle Asiento";
			List<DetalleAsiento> lista = new LinkedList<>();
			lista = detalleasientoDao.read();
			mv.addObject("msg", msg);
			mv.addObject("lista", lista);
			mv.setViewName("detalleasiento");
			return mv;
		} catch (Exception e) {
			System.out.println("error: " + e);
			error = "error al en tablaDetalleasiento desde " + this.getClass().getSimpleName();
			this.vista = "cuenta";
			this.errorHand(this.vista);
			return mv;
		}
	}
	
	@RequestMapping(value = "/Asiento", method = RequestMethod.GET)
	public ModelAndView asiento() {
		ModelAndView mv = new ModelAndView("asiento");
		try {
			List<Asiento> lista = new LinkedList<>();
			lista = asientoDao.read();
			mv.addObject("msg", msg);
			mv.addObject("lista", lista);
			mv.setViewName("asiento");
			return mv;
		} catch (Exception e) {
			System.out.println("error: " + e);
			error = "error al en tablaAsiento desde " + this.getClass().getSimpleName();
			this.vista = "asiento";
			this.errorHand(this.vista);
			return mv;
		}
	}
	
	@RequestMapping(value = "/RegistrarAsiento", method = RequestMethod.GET)
	public ModelAndView productosfacturas(@RequestParam("registro") String personas) {
		ModelAndView mv = new ModelAndView();
		try {

			List<Facturas> lista = new LinkedList<>();
			lista = facturasDao.consultarFacturasNoDespachadas();
			mv.addObject("msg", msg);
			mv.addObject("listafactura", lista);

			List<Ordenes> listap = new LinkedList<>();
			listap = ordenesDao.consultarProductosFacturasNoDespachadas(personas);
			String codigoOrden = listap.get(0).getCodigo();

			mv.addObject("codigoOrden", codigoOrden);
			mv.addObject("msg", msg);
			mv.addObject("listaproductos", listap);
			mv.setViewName("despachar_factura");
			return mv;
		} catch (Exception e) {
			System.out.println("error: " + e);
			error = "error al en tabla Ordenes desde " + this.getClass().getSimpleName();
			this.vista = "ordenes";
			// this.errorHand(this.vista);
			return mv;
		}

	}
	
	
	protected ModelAndView errorHand(String nombreVista) {
		ModelAndView mv = new ModelAndView();
		error = "Ocurrio un error al cargar la ultima accion :(";
		mv.addObject("error", error);
		mv.setViewName("index");
		return mv;
	}
	
	
	
}
