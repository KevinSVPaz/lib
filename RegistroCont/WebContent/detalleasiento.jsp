

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<meta charset="ISO-8859-1">
<title>Hi :D</title>
</head>
<body>
	<div class="card-panel">

		<div class="row">
			<div class="card z-depth-3 blue center">
				<h2 class="text-primary">Asientos</h2>
			</div>
		</div>
		<div class="row">
			<div class="card z-depth-3 center">
				<table class="striped">

					<thead class="blue">
						<tr>
							<th>Fecha</th>
							<th>Codigo</th>
							<th>Nombre</th>
							<th>Registro</th>
							<th>Debe</th>
							<th>Haber</th>

						</tr>
					</thead>

					<tbody>
						<fmt:setLocale value="en_US" />
						<c:forEach items="${lista}" var="bean">
							<c:choose>
								<c:when test="${bean.orden == 1}">
									<fmt:formatNumber value="${bean.valor}" currencySymbol="$" var="val"
										type="currency" />
									<tr>
										<td>${bean.asiento.fecha}</td>
										<td>${bean.cuenta.codigo}</td>
										<td>${bean.cuenta.nombre}</td>
										<td>${bean.asiento.asiento}</td>
										<td>${val}</td>
										<td></td>

									</tr>


								</c:when>
								<c:otherwise>
									<fmt:formatNumber value="${bean.valor}" currencySymbol="$" var="val2"
										type="currency" />
									<tr>
										<td>${bean.asiento.fecha}</td>
										<td>${bean.cuenta.codigo}</td>
										<td>${bean.cuenta.nombre}</td>
										<td>${bean.asiento.asiento}</td>
										<td></td>
										<td>${val2}</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</c:forEach>

					</tbody>

				</table>
			</div>
		</div>
	</div>

</body>
</html>