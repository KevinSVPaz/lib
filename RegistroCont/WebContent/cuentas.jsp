

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
<meta charset="ISO-8859-1">
<title>Hi :D</title>
</head>
<body>
	<div class="card-panel">

		<div class="row">
			<div class="card z-depth-3 blue center">
				<h2 class="text-primary">Cuentas</h2>
			</div>
		</div>
		<div class="row">
			<div class="card z-depth-3 center">
				<table class="striped">
				
					<thead class="blue">
						<tr>
							<th>Correlativo</th>
							<th>Nombre de Cuenta</th>
							<th>Codigo</th>
						</tr>
					</thead>
					
					<tbody>
						<c:forEach items="${lista}" var="bean">
							<tr>
								<td>${bean.idCuenta}</td>
								<td>${bean.nombre}</td>
								<td>${bean.codigo}</td>
							</tr>
						</c:forEach>
					</tbody>
					
				</table>
			</div>
		</div>
	</div>

</body>
</html>