<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>Startup - Horizontal Half</title>

<!-- Stylesheet -->
<link
	href="//cdn.shopify.com/s/files/1/1775/8583/t/1/assets/startup-materialize.min.css?100"
	rel="stylesheet">

<!-- Material Icons -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<script type="text/javascript" async=""
	src="https://cdn.shopify.com/s/javascripts/tricorder/trekkie.storefront.min.js?v=2019.11.04.1"></script>
<script>
	window.performance && window.performance.mark
			&& window.performance.mark('shopify.content_for_header.start');
</script>
<meta id="shopify-digital-wallet" name="shopify-digital-wallet"
	content="/17758583/digital_wallets/dialog">
<meta name="shopify-checkout-api-token"
	content="6aacc581eb2b41d74f03c38d3c985dba">
<meta id="in-context-paypal-metadata" data-shop-id="17758583"
	data-venmo-supported="true" data-environment="production"
	data-locale="en_US" data-paypal-v4="true" data-currency="USD">
<style media="all">
.additional-checkout-button {
	border: 0 !important;
	border-radius: 5px !important;
	display: inline-block;
	margin: 0 0 10px;
	padding: 0 24px !important;
	max-width: 100%;
	min-width: 150px !important;
	line-height: 44px !important;
	text-align: center !important
}

.additional-checkout-button+.additional-checkout-button {
	margin-left: 10px
}

.additional-checkout-button:last-child {
	margin-bottom: 0
}

.additional-checkout-button span {
	font-size: 14px !important
}

.additional-checkout-button img {
	display: inline-block !important;
	height: 1.3em !important;
	margin: 0 !important;
	vertical-align: middle !important;
	width: auto !important
}

@media ( max-width : 500px) {
	.additional-checkout-button {
		display: block;
		margin-left: 0 !important;
		padding: 0 10px !important;
		width: 100%
	}
}

.additional-checkout-button--apple-pay {
	background-color: #000 !important;
	color: #fff !important;
	display: none;
	font-family: -apple-system, Helvetica Neue, sans-serif !important;
	min-width: 150px !important;
	white-space: nowrap !important
}

.additional-checkout-button--apple-pay:hover,
	.additional-checkout-button--apple-pay:active,
	.additional-checkout-button--apple-pay:visited {
	color: #fff !important;
	text-decoration: none !important
}

.additional-checkout-button--apple-pay .additional-checkout-button__logo
	{
	background: -webkit-named-image(apple-pay-logo-white) center center
		no-repeat !important;
	background-size: auto 100% !important;
	display: inline-block !important;
	vertical-align: middle !important;
	width: 3em !important;
	height: 1.3em !important
}

@media ( max-width : 500px) {
	.additional-checkout-button--apple-pay {
		display: none
	}
}

.additional-checkout-button--google-pay {
	line-height: 0 !important;
	padding: 0 !important;
	border-radius: unset !important;
	width: 80px !important
}

@media ( max-width : 500px) {
	.additional-checkout-button--google-pay {
		width: 100% !important
	}
}

.gpay-iframe {
	height: 44px !important;
	width: 100% !important;
	cursor: pointer;
	vertical-align: middle !important
}

.additional-checkout-button--paypal-express {
	background-color: #ffc439 !important
}

.additional-checkout-button--paypal, .additional-checkout-button--venmo
	{
	vertical-align: top;
	line-height: 0 !important;
	padding: 0 !important
}

.additional-checkout-button--amazon {
	background-color: #fad676 !important;
	position: relative !important
}

.additional-checkout-button--amazon .additional-checkout-button__logo {
	-webkit-transform: translateY(4px) !important;
	transform: translateY(4px) !important
}

.additional-checkout-button--amazon .alt-payment-list-amazon-button-image
	{
	max-height: none !important;
	opacity: 0 !important;
	position: absolute !important;
	top: 0 !important;
	left: 0 !important;
	width: 100% !important;
	height: 100% !important
}

.additional-checkout-button-visually-hidden {
	border: 0 !important;
	clip: rect(0, 0, 0, 0) !important;
	clip: rect(0, 0, 0, 0) !important;
	width: 1px !important;
	height: 1px !important;
	margin: -2px !important;
	overflow: hidden !important;
	padding: 0 !important;
	position: absolute !important
}
</style>
<script id="apple-pay-shop-capabilities" type="application/json">{"shopId":17758583,"countryCode":"US","currencyCode":"USD","merchantCapabilities":["supports3DS"],"merchantId":"gid:\/\/shopify\/Shop\/17758583","merchantName":"Materialize Themes","requiredBillingContactFields":["postalAddress","email"],"requiredShippingContactFields":["postalAddress","email"],"shippingType":"shipping","supportedNetworks":["visa","masterCard","amex","discover"],"total":{"type":"pending","label":"Materialize Themes","amount":"1.00"}}</script>
<script id="shopify-features" type="application/json">{"accessToken":"6aacc581eb2b41d74f03c38d3c985dba","betas":[],"domain":"themes.materializecss.com","predictiveSearch":true,"shopId":17758583,"smart_payment_buttons_url":"https:\/\/cdn.shopify.com\/shopifycloud\/payment-sheet\/assets\/latest\/spb.en.js","dynamic_checkout_cart_url":"https:\/\/cdn.shopify.com\/shopifycloud\/payment-sheet\/assets\/latest\/dynamic-checkout-cart.en.js","locale":"en"}</script>
<script>
	var Shopify = Shopify || {};
	Shopify.shop = "materialize-themes.myshopify.com";
	Shopify.currency = {
		"active" : "USD",
		"rate" : "1.0"
	};
	Shopify.theme = {
		"name" : "debut",
		"id" : 133945025,
		"theme_store_id" : 796,
		"role" : "main"
	};
	Shopify.theme.handle = "null";
	Shopify.theme.style = {
		"id" : null,
		"handle" : null
	};
</script>
<script type="module">!function(o){(o.Shopify=o.Shopify||{}).modules=!0}(window);</script>
<script>
	!function(o) {
		function n() {
			var o = [];
			function n() {
				o.push(Array.prototype.slice.apply(arguments))
			}
			return n.q = o, n
		}
		var t = o.Shopify = o.Shopify || {};
		t.loadFeatures = n(), t.autoloadFeatures = n()
	}(window);
</script>
<script>
	window.ShopifyPay = window.ShopifyPay || {};
	window.ShopifyPay.apiHost = "pay.shopify.com";
</script>
<script id="__st">
	var __st = {
		"a" : 17758583,
		"offset" : -28800,
		"reqid" : "c39e642b-b43d-4237-97e9-d47b950ae260",
		"pageurl" : "themes.materializecss.com\/pages\/startup-horizontal-half.html",
		"s" : "pages-203794817",
		"u" : "72a10d5036fb",
		"p" : "page",
		"rtyp" : "page",
		"rid" : 203794817
	};
</script>
<script>
	window.ShopifyPaypalV4VisibilityTracking = true;
</script>
<script>
	window.ShopifyAnalytics = window.ShopifyAnalytics || {};
	window.ShopifyAnalytics.meta = window.ShopifyAnalytics.meta || {};
	window.ShopifyAnalytics.meta.currency = 'USD';
	var meta = {
		"page" : {
			"pageType" : "page",
			"resourceType" : "page",
			"resourceId" : 203794817
		}
	};
	for ( var attr in meta) {
		window.ShopifyAnalytics.meta[attr] = meta[attr];
	}
</script>
<script>
	window.ShopifyAnalytics.merchantGoogleAnalytics = function() {

	};
</script>
<script class="analytics">
	(window.gaDevIds = window.gaDevIds || []).push('BwiEti');

	(function() {
		var customDocumentWrite = function(content) {
			var jquery = null;

			if (window.jQuery) {
				jquery = window.jQuery;
			} else if (window.Checkout && window.Checkout.$) {
				jquery = window.Checkout.$;
			}

			if (jquery) {
				jquery('body').append(content);
			}
		};

		var isDuplicatedThankYouPageView = function() {
			return document.cookie.indexOf('loggedConversion='
					+ window.location.pathname) !== -1;
		}

		var setCookieIfThankYouPage = function() {
			if (window.location.pathname.indexOf('/checkouts') !== -1
					&& window.location.pathname.indexOf('/thank_you') !== -1) {

				var twoMonthsFromNow = new Date(Date.now());
				twoMonthsFromNow.setMonth(twoMonthsFromNow.getMonth() + 2);

				document.cookie = 'loggedConversion='
						+ window.location.pathname + '; expires='
						+ twoMonthsFromNow;
			}
		}

		var trekkie = window.ShopifyAnalytics.lib = window.trekkie = window.trekkie
				|| [];
		if (trekkie.integrations) {
			return;
		}
		trekkie.methods = [ 'identify', 'page', 'ready', 'track', 'trackForm',
				'trackLink' ];
		trekkie.factory = function(method) {
			return function() {
				var args = Array.prototype.slice.call(arguments);
				args.unshift(method);
				trekkie.push(args);
				return trekkie;
			};
		};
		for (var i = 0; i < trekkie.methods.length; i++) {
			var key = trekkie.methods[i];
			trekkie[key] = trekkie.factory(key);
		}
		trekkie.load = function(config) {
			trekkie.config = config;
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.onerror = function(e) {
				(new Image()).src = '//v.shopify.com/internal_errors/track?error=trekkie_load';
			};
			script.async = true;
			script.src = 'https://cdn.shopify.com/s/javascripts/tricorder/trekkie.storefront.min.js?v=2019.11.04.1';
			var first = document.getElementsByTagName('script')[0];
			first.parentNode.insertBefore(script, first);
		};
		trekkie.load({
			"Trekkie" : {
				"appName" : "storefront",
				"development" : false,
				"defaultAttributes" : {
					"shopId" : 17758583,
					"isMerchantRequest" : null,
					"themeId" : 133945025,
					"themeCityHash" : "1670185919760860438",
					"contentLanguage" : "en",
					"currency" : "USD"
				}
			},
			"Performance" : {
				"navigationTimingApiMeasurementsEnabled" : true,
				"navigationTimingApiMeasurementsSampleRate" : 1
			},
			"Google Analytics" : {
				"trackingId" : "UA-56218128-1",
				"domain" : "auto",
				"siteSpeedSampleRate" : "10",
				"enhancedEcommerce" : true,
				"doubleClick" : true,
				"includeSearch" : true
			},
			"Session Attribution" : {}
		});

		var loaded = false;
		trekkie
				.ready(function() {
					if (loaded)
						return;
					loaded = true;

					window.ShopifyAnalytics.lib = window.trekkie;

					ga('require', 'linker');
					function addListener(element, type, callback) {
						if (element.addEventListener) {
							element.addEventListener(type, callback);
						} else if (element.attachEvent) {
							element.attachEvent('on' + type, callback);
						}
					}
					function decorate(event) {
						event = event || window.event;
						var target = event.target || event.srcElement;
						if (target
								&& (target.getAttribute('action') || target
										.getAttribute('href'))) {
							ga(function(tracker) {
								var linkerParam = tracker.get('linkerParam');
								document.cookie = '_shopify_ga=' + linkerParam
										+ '; ' + 'path=/';
							});
						}
					}
					addListener(window, 'load', function() {
						for (var i = 0; i < document.forms.length; i++) {
							var action = document.forms[i]
									.getAttribute('action');
							if (action && action.indexOf('/cart') >= 0) {
								addListener(document.forms[i], 'submit',
										decorate);
							}
						}
						for (var i = 0; i < document.links.length; i++) {
							var href = document.links[i].getAttribute('href');
							if (href && href.indexOf('/checkout') >= 0) {
								addListener(document.links[i], 'click',
										decorate);
							}
						}
					});

					var originalDocumentWrite = document.write;
					document.write = customDocumentWrite;
					try {
						window.ShopifyAnalytics.merchantGoogleAnalytics
								.call(this);
					} catch (error) {
					}
					;
					document.write = originalDocumentWrite;
					(function() {
						if (window.BOOMR
								&& (window.BOOMR.version || window.BOOMR.snippetExecuted)) {
							return;
						}
						window.BOOMR = window.BOOMR || {};
						window.BOOMR.snippetStart = new Date().getTime();
						window.BOOMR.snippetExecuted = true;
						window.BOOMR.snippetVersion = 12;
						window.BOOMR.shopId = 17758583;
						window.BOOMR.themeId = 133945025;
						window.BOOMR.url = "https://cdn.shopify.com/shopifycloud/boomerang/shopify-boomerang-1.0.0.min.js";
						var where = document.currentScript
								|| document.getElementsByTagName("script")[0];
						var parentNode = where.parentNode;
						var promoted = false;
						var LOADER_TIMEOUT = 3000;
						function promote() {
							if (promoted) {
								return;
							}
							var script = document.createElement("script");
							script.id = "boomr-scr-as";
							script.src = window.BOOMR.url;
							script.async = true;
							parentNode.appendChild(script);
							promoted = true;
						}
						function iframeLoader(wasFallback) {
							promoted = true;
							var dom, bootstrap, iframe, iframeStyle;
							var doc = document;
							var win = window;
							window.BOOMR.snippetMethod = wasFallback ? "if"
									: "i";
							bootstrap = function(parent, scriptId) {
								var script = doc.createElement("script");
								script.id = scriptId || "boomr-if-as";
								script.src = window.BOOMR.url;
								BOOMR_lstart = new Date().getTime();
								parent = parent || doc.body;
								parent.appendChild(script);
							};
							if (!window.addEventListener && window.attachEvent
									&& navigator.userAgent.match(/MSIE [67]./)) {
								window.BOOMR.snippetMethod = "s";
								bootstrap(parentNode, "boomr-async");
								return;
							}
							iframe = document.createElement("IFRAME");
							iframe.src = "about:blank";
							iframe.title = "";
							iframe.role = "presentation";
							iframe.loading = "eager";
							iframeStyle = (iframe.frameElement || iframe).style;
							iframeStyle.width = 0;
							iframeStyle.height = 0;
							iframeStyle.border = 0;
							iframeStyle.display = "none";
							parentNode.appendChild(iframe);
							try {
								win = iframe.contentWindow;
								doc = win.document.open();
							} catch (e) {
								dom = document.domain;
								iframe.src = "javascript:var d=document.open();d.domain='"
										+ dom + "';void(0);";
								win = iframe.contentWindow;
								doc = win.document.open();
							}
							if (dom) {
								doc._boomrl = function() {
									this.domain = dom;
									bootstrap();
								};
								doc
										.write("<body onload='document._boomrl();'>");
							} else {
								win._boomrl = function() {
									bootstrap();
								};
								if (win.addEventListener) {
									win.addEventListener("load", win._boomrl,
											false);
								} else if (win.attachEvent) {
									win.attachEvent("onload", win._boomrl);
								}
							}
							doc.close();
						}
						var link = document.createElement("link");
						if (link.relList
								&& typeof link.relList.supports === "function"
								&& link.relList.supports("preload")
								&& ("as" in link)) {
							window.BOOMR.snippetMethod = "p";
							link.href = window.BOOMR.url;
							link.rel = "preload";
							link.as = "script";
							link.addEventListener("load", promote);
							link.addEventListener("error", function() {
								iframeLoader(true);
							});
							setTimeout(function() {
								if (!promoted) {
									iframeLoader(true);
								}
							}, LOADER_TIMEOUT);
							BOOMR_lstart = new Date().getTime();
							parentNode.appendChild(link);
						} else {
							iframeLoader(false);
						}
						function boomerangSaveLoadTime(e) {
							window.BOOMR_onload = (e && e.timeStamp)
									|| new Date().getTime();
						}
						if (window.addEventListener) {
							window.addEventListener("load",
									boomerangSaveLoadTime, false);
						} else if (window.attachEvent) {
							window.attachEvent("onload", boomerangSaveLoadTime);
						}
						if (document.addEventListener) {
							document.addEventListener("onBoomerangLoaded",
									function(e) {
										e.detail.BOOMR.init({});
										e.detail.BOOMR.t_end = new Date()
												.getTime();
									});
						} else if (document.attachEvent) {
							document
									.attachEvent(
											"onpropertychange",
											function(e) {
												if (!e)
													e = event;
												if (e.propertyName === "onBoomerangLoaded") {
													e.detail.BOOMR.init({});
													e.detail.BOOMR.t_end = new Date()
															.getTime();
												}
											});
						}
					})();

					if (!isDuplicatedThankYouPageView()) {
						setCookieIfThankYouPage();

						window.ShopifyAnalytics.lib.page(null, {
							"pageType" : "page",
							"resourceType" : "page",
							"resourceId" : 203794817
						});

					}
				});

		var eventsListenerScript = document.createElement('script');
		eventsListenerScript.async = true;
		eventsListenerScript.src = "//cdn.shopify.com/s/assets/shop_events_listener-09875a9a2b286acf534498184c24b199675a6097a941992d0979e5295d2cf9e9.js";
		document.getElementsByTagName('head')[0]
				.appendChild(eventsListenerScript);

	})();
</script>
<script async=""
	src="//cdn.shopify.com/s/assets/shop_events_listener-09875a9a2b286acf534498184c24b199675a6097a941992d0979e5295d2cf9e9.js"></script>
<script integrity="sha256-mO+GKBT+KVLtCJOxhHda/n8GRk8f8i7hhza2QxpsYxc="
	crossorigin="anonymous" data-source-attribution="shopify.loadfeatures"
	defer="defer"
	src="//cdn.shopify.com/s/assets/storefront/load_feature-98ef862814fe2952ed0893b184775afe7f06464f1ff22ee18736b6431a6c6317.js"></script>
<script crossorigin="anonymous" defer="defer"
	src="//cdn.shopify.com/s/assets/shopify_pay/storefront-f95c62afca18778ed8677facd32818c864b5e4938cba1769e8d8ba0b541d41dc.js?v=20190107"></script>
<script integrity="sha256-EYppj7RbseKnaugbP4EJXR4sMs7TPdTpPmQ3i163eNA="
	data-source-attribution="shopify.dynamic-checkout" defer="defer"
	src="//cdn.shopify.com/s/assets/storefront/features-118a698fb45bb1e2a76ae81b3f81095d1e2c32ced33dd4e93e64378b5eb778d0.js"
	crossorigin="anonymous"></script>
<script defer="defer"
	src="//cdn.shopify.com/s/assets/themes_support/ga_urchin_forms-99e991855b3d8ddc69e625c68ab0579dd9927b611c2ec4943d396c72e3af0849.js"></script>


<style id="shopify-dynamic-checkout-cart">
@media screen and (min-width: 750px) {
	#dynamic-checkout-cart {
		min-height: 50px;
	}
}

@media screen and (max-width: 750px) {
	#dynamic-checkout-cart {
		min-height: 240px;
	}
}
</style>
<script>
	window.performance && window.performance.mark
			&& window.performance.mark('shopify.content_for_header.end');
</script>
<link rel="canonical"
	href="https://themes.materializecss.com/pages/startup-horizontal-half.html">
<link
	href="https://cdn.shopify.com/shopifycloud/boomerang/shopify-boomerang-1.0.0.min.js"
	rel="preload" as="script">
<script id="boomr-scr-as"
	src="https://cdn.shopify.com/shopifycloud/boomerang/shopify-boomerang-1.0.0.min.js"
	async=""></script>
</head>

<body style="">

	<ul id="slide-out" class="side-nav"
		style="transform: translateX(-100%);">
		<li class="no-padding">
			<ul class="collapsible collapsible-accordion">
				<li class="bold active"><a
					class="collapsible-header waves-effect waves-teal active">Pages</a>
					<div class="collapsible-body" style="display: block;">
						<ul>
							<li><a class="g" href="startup-horizontal-half.html">Horizontal
									Halves</a></li>
							<li><a href="startup-zoom-out.html">Zoom Out</a></li>
							<li><a href="startup-circle-reveal.html">Circle Reveal</a></li>
							<li><a href="startup-phone-wall.html">Phone Wall</a></li>
							<li><a href="startup-element-transitions.html">Element
									Transitions</a></li>
							<li><a href="startup-basic-elements.html">Basic Elements</a></li>
							<li><a href="startup-shuffle.html">Shuffle</a></li>
							<li><a href="startup-postcard.html">Postcards</a></li>
						</ul>
					</div></li>
			</ul>
		</li>
		<li><a class="waves-effect waves-teal" href="startup-blog.html">Blog</a></li>
		<li><a class="waves-effect waves-teal" href="startup-team.html">Team</a></li>
		<li><a class="waves-effect waves-teal" href="#">Buy Now!</a></li>
	</ul>

	<div data-scrollmagic-pin-spacer="" class="scrollmagic-pin-spacer"
		style="top: auto; left: auto; bottom: auto; right: auto; margin: 0px; display: block; position: relative; box-sizing: content-box; height: 100%; padding-top: 2700px; padding-bottom: 1245px;">
		<div class="ai full-height fh" data-id="1"
			style="position: fixed; margin: auto; top: 0px; left: 0px; bottom: auto; right: auto; box-sizing: border-box; width: 100%; height: 100%;"
			data-disabled="false">
			<div class="bz">
				<img
					src="//cdn.shopify.com/s/files/1/1775/8583/t/1/assets/poly5.jpg?100"
					alt="">
			</div>
			<div class="phone-preview-sizer" style="left: 34.4138%;">
				<div class="phone-preview"></div>
				<div class="image-container"
					style="background-image: url(https://lisajoyphillips.com/wp-content/uploads/2019/08/IMG_7771-1-768x1024.jpg)"></div>
				<div class="image-container"
					style="background-image: url(https://lh3.googleusercontent.com/proxy/lfSm9O3zfbDA1eSLwlDs3qOzA0fy3EOAL02URQuTSHcXBU0DSkQ60mf1DdKPD1mgu3UbNvFgInKzNCJSIvfImyUhYAxlI5Ph3aqYLn2aIvQTQ_f8KfiY_PDcDtIiEq1viUvOdA)"></div>
				<div class="image-container g"
					style="background-image: url(https://static-30.sinclairstoryline.com/resources/media/9c901286-3c7f-4baf-a9be-4b55c0bb92cc-large3x4_money.jpg?1557414396566)"></div>
			</div>
			<div class="horizontal-half-wrapper he g" style="z-index: -1;">
				<div class="header-background l" style="right: -100%;"></div>
				<div class="header-wrapper c valign-wrapper"
					style="right: -50px; opacity: 0;">
					<div class="d iv kw offset-m2 ci">
						<h1>Cuentas</h1>
						<span class="dx">Administra tus cuentas ahora mismo</span>
						<button class="read-more">
							<i class="icon-caret-down"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="horizontal-half-wrapper" style="z-index: -1;">
				<div class="header-background l" style="left: -100.998%;"></div>
				<div class="header-wrapper c valign-wrapper"
					style="left: -50px; opacity: 0;">
					<div class="d iv kw offset-m2 ci">
						<h1>Asientos Contables</h1>
						<span class="dx">Administre todas las acciones de entrada y
							salida de dinero de su empresa</span>
						<button class="read-more">
							<i class="icon-caret-down"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="horizontal-half-wrapper he" style="z-index: 1;">
				<div class="header-background l" style="right: -75.6137%;"></div>
				<div class="header-wrapper c valign-wrapper" style="">
					<div class="d iv kw offset-m2 ci">
						<h1>Detalle</h1>
						<span class="dx">Asigne todo ese dinero en movimiento a una
							cuenta</span>
						<button class="read-more">
							<i class="icon-caret-down"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="v valign-wrapper">
		<div class="c ci">
			<div class="d iv jc offset-m1">
				<div class="c">
					<div class="d iv">
						<h2 class="section-title">Opciones</h2>
					</div>

					<div class="d iv jo kk">
						<h4>
							<i class="icon-light-bulb" style="padding-left: 190px"></i>
						</h4>
						<div class="row">
							<div class=" z-depth-3  btn2 center">
								<button class="btn btn2 black">
									<p class="ek">Vea todas las cuentas ingresadas en el
										sistema</p>
									<a href="verCuentas">ver el Catalogo de Cuentas</a>
								</button>
							</div>
						</div>
					</div>

					<div class="d iv jo kk"">
						<h4>
							<i class="icon-bolt" style="padding-left: 200px"></i>
						</h4>
						<button class="btn btn2 black">
							<p class="ek">Vea el movimiento de los asientos contables de
								las empresas</p>
							<a href="verDetalleAsiento">Ver los Asientos Contables</a>
						</button>
					</div>

					<div class="d iv jo kk">
						<h4>
							<i class="icon-rocket" style="padding-left: 200px"></i>
						</h4>
						<p class="ek">Vea el movimiento de los asientos contables de
								las empresas</p>
							<a href="verDetalleAsiento">Ver los Asientos Contables</a>
					</div>

				</div>
			</div>
		</div>
	</div>


	<!-- Scripts -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/materialize/0.98.2/js/materialize.min.js"></script>
	<script
		src="//cdn.shopify.com/s/files/1/1775/8583/t/1/assets/startup-all-min.js?100"
		crossorigin="anonymous"></script>


	<div class="hiddendiv common"></div>
	<div class="drag-target" data-sidenav="slide-out"
		style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>
</html>